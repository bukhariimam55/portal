@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading">DATA USER</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="table-responsive">
                    <table border="1" class="table table-responsive">
                      <tr>
                        <td width="5" align="center"><strong>No</strong></td>
                        <td align="center"><strong>Tanggal</strong></td>
                        <td align="center"><strong>ID User</strong></td>
                        <td align="center"><strong>Nama</strong></td>
                        <td align="center"><strong>No Hp</strong></td>
                        <td align="center"><strong>Saldo</strong></td>

                      </tr>
                      <?php $id=0; ?>
                      @foreach($users as $key)
                      <?php $id+=1; ?>
                      <tr>
                        <td>{{$id}}</td>
                        <td align="center">{{ $key->created_at}}</td>
                        <td align="center">ID-{{ $key->id}}</td>
                        <td align="center">{{ $key->name}}</td>
                        <td align="center">{{ $key->hp }}</td>
                        <td align="right">{{ number_format($key->saldo)}}</td>
                      </tr>
                      @endforeach
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
