<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'v1'], function () {
    Route::post('callback', 'Api\ResponseController@callback');
    Route::post('agent', 'Api\CallbackController@agent');
    Route::post('user/signin', 'Api\PassportController@login');
    Route::post('user/register', 'Api\PassportController@register');
    Route::get('12345/6789', 'Api\AuthController@cek');
    Route::get('listvoucherlistrik', 'Api\CekController@listvoucherlistrik');
    Route::get('datatransaksi{id}', 'Api\CekController@datatransaksi');
    Route::get('datasaldo{id}', 'Api\CekController@datasaldo');
    Route::get('hargapulsa/{id}', 'Api\CekController@hargapulsa');



    Route::get('provider', 'Api\CekController@provider');

    Route::post('testback', 'Api\ResponseController@testback');
});
Route::group(['prefix' => 'v1','middleware'=>'auth:api'], function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
    Route::post('user/logout', 'Api\PassportController@logout');
    Route::post('get-details', 'Api\PassportController@getDetails');
    Route::post('belipaket', 'Api\TransactionController@belipaket');
    Route::post('prosespulsa', 'Api\ProsesController@prosespulsa');
    Route::post('isisaldo', 'Api\ProsesController@tambahsaldo');
    Route::post('updateproduct', 'Api\ProsesController@updateproduct');
    Route::post('editpass', 'Api\ProsesController@editpass');
    //CekController
    Route::post('prov_token', 'Api\CekController@prov_token');
    Route::post('cekharga', 'Api\CekController@cekharga');
    Route::post('detailPln', 'Api\CekController@detailPln');
    Route::post('hargavoucherlistrik', 'Api\CekController@hargavoucherlistrik');
    Route::post('cekstatus', 'Api\CekController@cekStatus');
    //Laundry
    Route::post('paketcuci', 'Api\PassportController@paketcuci');
    //ROUTE AGEN
    Route::post('proses-agent', 'Api\SuplayerController@prosesagent');
    Route::post('topup-suplayer', 'Api\SuplayerController@suplayer');
    Route::post('data-suplayer', 'Api\SuplayerController@datasuplayer');
    Route::post('buku-saldo-suplayer', 'Api\SuplayerController@bukusaldosuplayer');
    Route::post('cek-status', 'Api\SuplayerController@cek_status_agent');
    Route::post('cek-pln-pascabayar', 'Api\PlnPascaController@plnpascabayar');
    Route::get('hargaagent/{id}', 'Api\SuplayerController@hargaagent');
});
