<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>FIXPAY</title>

    <!-- Styles -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>

  <?php

$show = App\Apis::where('active',1)->first();
$url = $show->url_server;
$header = array(
'h2h-userid:'.$show->userid,
'h2h-key: '.$show->key, // lihat hasil autogenerate di member area
'h2h-secret:'.$show->secret, // lihat hasil autogenerate di member area
);

$data = array(
'inquiry' => 'S', // konstan
);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
$result = curl_exec($ch);

 $json = json_decode($result, true);?>
    <div id="app">
      <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
          <div class="container">
                <div class="navbar-header">



                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        FIXPAY
                    </a>
                    <!-- Collapsed Hamburger -->
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest

                        @else
                          @if(Auth::user()->tipe =="user")
                          @else

                            <li><a class="nav-link" href="{{ url('/home') }}">HOME</a></li>
                            <li><a class="nav-link" href="{{ url('/users') }}">DATA USERS</a></li>
                            <li class="nav-item dropdown">
                              <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>DEPOSIT <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                  <li><a class="dropdown-item" href="{{ url('/condef') }}">Konfirmasi Deposit</a></li>
                                  <li><a class="dropdown-item" href="{{ url('/konfirmasi-deposit-agen') }}">Konfirmasi Deposit Agen</a></li>
                                  <li><a class="dropdown-item" href="{{ url('/datadeposit') }}">Data Deposit</a></li>
                                  <li><a class="dropdown-item" href="{{ route('bukusaldo') }}">Buku Saldo Agen</a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                              <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>DATA TRANSAKSI <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                  <li><a class="dropdown-item" href="{{ url('/datpul') }}">Transaksi NonAgen</a></li>
                                  <li><a class="dropdown-item" href="{{ url('/transaksi-agent') }}">Transaksi Agen</a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                              <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>DATA HARGA <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                  <li><a class="dropdown-item" href="{{ url('/dathar') }}">Data Harga USer</a></li>
                                  <li><a class="dropdown-item" href="{{ url('/agent') }}">Data Harga Agen</a></li>
                                </ul>
                            </li>
                          @endif
                          @include('flash::message')
                            <li class="nav-item dropdown">

                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="#">Saldo : {{ number_format($json['balance'])}}</a></li> {{--{{ number_format($json['balance'])}}--}}
                                    <li>
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            LOGOUT
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>

                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="//code.jquery.com/jquery.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>
