<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agen extends Model
{
  protected $fillable = [
      'id','operator','description','code','price','status','provider_sub','untung'
  ];
  protected $hidden = [
      'created_at','updated_at'//,'code','price','status','provider_sub','untung'
  ];
}
