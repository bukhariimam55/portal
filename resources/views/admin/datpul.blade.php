@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading">DATA PULSA</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="table-responsive">
                    <table border="1" class="table table-responsive">
                      <tr>
                        <td width="5" align="center"><strong>No</strong></td>
                        <td align="center"><strong>Tangal</strong></td>
                        <td align="center"><strong>ID User</strong></td>
                        <td align="center"><strong>Paket</strong></td>
                        <td align="center"><strong>Nomor Hp</strong></td>
                        <td align="center"><strong>Nominal</strong></td>
                        <td align="center"><strong>Saldo Akhir</strong></td>
                        <td align="center"><strong>Status</strong></td>
                      </tr>
                      <?php $id=0; ?>
                      @foreach($transaction as $key)
                      <?php $id+=1; ?>
                      <tr>
                        <td>{{$id}}</td>
                        <td align="center">{{ $key->created_at}}</td>
                        <td align="center">{{ $key->userid}}</td>
                        <td align="center">{{ $key->paket}}</td>
                        <td align="center">{{ $key->hp_id_pel}}</td>
                        <td align="right">{{ number_format($key->harga)}}</td>
                        <td align="right">{{ number_format($key->saldo)}}</td>
                        <td align="center">{{ $key->status}}</td>
                      </tr>
                      @endforeach
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
